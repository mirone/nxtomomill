#!/bin/bash

function install_tomoscan(){
    python -m pip install git+https://gitlab.esrf.fr/tomotools/tomoscan.git@add_current
}

function install_silx(){
    python -m pip install git+https://github.com/silx-kit/silx.git@master
}

function silx_version(){
    python -c 'import silx; print(silx.version)'
}
