# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/09/2021"


import pytest
from nxtomomill import plugins
from nxtomomill.test.utils.bliss import MockBlissAcquisition
from nxtomomill.app.h52nx import main
import contextlib
import os
import tempfile
import h5py


class EnvContext(contextlib.AbstractContextManager):
    def __init__(self, **dicts):
        self._keys = dicts
        self._old_values = {}

    def __enter__(self):
        self._old_values.clear()
        for key, new_value in self._keys.items():
            self._old_values[key] = os.environ.get(key, None)
            if new_value is None:
                if key in os.environ:
                    del os.environ[key]
            else:
                os.environ[key] = new_value

    def __exit__(self, *exec):
        for key, old_value in self._old_values.items():
            if old_value is None:
                if key in os.environ:
                    del os.environ[key]
            else:
                os.environ[key] = old_value


def test_get_plugins_instances_frm_env_var():

    with EnvContext(NXTOMOMILL_PLUGINS_DIR="/unknow/path"):
        with pytest.raises(ValueError):
            plugins.get_plugins_instances_frm_env_var()

    with EnvContext(NXTOMOMILL_PLUGINS_DIR=None):
        with pytest.raises(ValueError):
            plugins.get_plugins_instances_frm_env_var()


def test_get_plugins_instances():
    with tempfile.TemporaryDirectory() as plugin_folder:
        my_plug_file = os.path.join(plugin_folder, "my_plugin.py")
        with open(my_plug_file, "w") as outfile:
            outfile.write(
                """
from nxtomomill.plugins import HDF5Plugin
import numpy

class PluginTest(HDF5Plugin):

    def __init__(self):
        super().__init__(
            name="plugin test",
            positioner_keys=("sy", ),
        )

    def update_nx_tomo(self, nx_tomo):
        try:
            sy = self.get_positioner_info("sy")
        except Exception as e:
            print(e)
        else:
            try:
                nx_tomo.sample.y_translation = numpy.ones(numpy.array(sy).shape)
            except Exception as e:
                print(e)
            """
            )
        with EnvContext(NXTOMOMILL_PLUGINS_DIR=plugin_folder):
            assert len(plugins.get_plugins_instances_frm_env_var()) == 1
            with tempfile.TemporaryDirectory() as folder:
                nx_file_path = os.path.join(folder, "acquisition.nx")

                bliss_mock = MockBlissAcquisition(
                    n_sample=2,
                    n_sequence=1,
                    n_scan_per_sequence=10,
                    n_darks=5,
                    n_flats=5,
                    with_nx_detector_attr=True,
                    output_dir=folder,
                    detector_name="pcolinux",
                )

                h5_file_path = bliss_mock.samples[0].sample_file
                assert not os.path.exists(nx_file_path), "outputfile exists already"
                main(["h52nx", h5_file_path, nx_file_path, "--single-file"])
                assert os.path.exists(nx_file_path), "outputfile doesn't exists"
                with h5py.File(nx_file_path, mode="r") as h5f:
                    assert h5f["entry0000/sample/y_translation"][()].min() == 1
                    assert h5f["entry0000/sample/y_translation"][()].max() == 1
