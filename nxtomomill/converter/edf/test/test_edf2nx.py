# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "26/04/2022"


import shutil
import tempfile
import os
from nxtomomill import converter
from nxtomomill.converter.edf.edfconverter import TomoEDFConfig
from tomoscan.esrf.mock import MockEDF
import numpy

from tomoscan.esrf.utils import dump_info_file

try:
    from tomoscan.esrf.scan.hdf5scan import HDF5TomoScan
    from tomoscan.esrf.scan.edfscan import EDFTomoScan
except ImportError:
    from tomoscan.esrf.hdf5scan import HDF5TomoScan
    from tomoscan.esrf.edfscan import EDFTomoScan
from tomoscan import version
from tomoscan.validator import is_valid_for_reconstruction
from nxtomomill.utils import Progress
import pytest


@pytest.mark.parametrize("progress", (None, Progress("conversion from edf")))
@pytest.mark.skipif(
    condition=(version.MINOR < 7 and version.MAJOR == 0),
    reason="dark_n and ref_n from EDFTomoScan where not existing",
)
def test_edf_to_nx_converter(progress):
    with tempfile.TemporaryDirectory() as folder:
        scan_path = os.path.join(folder, "myscan")
        n_proj = 120
        n_alignment_proj = 5
        dim = 100
        MockEDF(
            scan_path=scan_path,
            n_radio=n_proj,
            n_ini_radio=n_proj,
            n_extra_radio=n_alignment_proj,
            dim=dim,
            dark_n=1,
            ref_n=1,
        )
        scan = EDFTomoScan(scan_path)
        assert scan.dark_n == 1
        output_file = os.path.join(folder, "nexus_file.nx")

        config = TomoEDFConfig()
        config.input_folder = scan_path
        config.output_file = output_file
        nx_file, nx_entry = converter.from_edf_to_nx(
            configuration=config,
            progress=progress,
        )
        hdf5_scan = HDF5TomoScan(scan=nx_file, entry=nx_entry)
        assert len(hdf5_scan.projections) == n_proj
        assert len(hdf5_scan.alignment_projections) == n_alignment_proj
        assert hdf5_scan.dim_1 == dim
        assert hdf5_scan.dim_2 == dim
        assert is_valid_for_reconstruction(hdf5_scan)


@pytest.mark.parametrize("scan_range", (-180, 180, 360))
@pytest.mark.parametrize("endpoint", (True, False))
@pytest.mark.parametrize("revert", (True, False))
@pytest.mark.parametrize("force_angles", (True, False))
def test_rotation_angle_infos(scan_range, endpoint, revert, force_angles):
    """test conversion fits TomoEDFConfig parameters regarding the rotation angle calculation options"""
    with tempfile.TemporaryDirectory() as data_folder:
        scan_path = os.path.join(data_folder, "myscan")
        n_proj = 12
        n_alignment_proj = 5
        dim = 4

        MockEDF(
            scan_path=scan_path,
            n_radio=n_proj,
            n_ini_radio=n_proj,
            n_extra_radio=n_alignment_proj,
            dim=dim,
            dark_n=1,
            ref_n=1,
            scan_range=scan_range,
            rotation_angle_endpoint=endpoint,
        )

        output_file = os.path.join(data_folder, "nexus_file.nx")

        config = TomoEDFConfig()
        config.input_folder = scan_path
        config.output_file = output_file
        config.force_angle_calculation = force_angles
        config.force_angle_calculation_endpoint = endpoint
        config.angle_calculation_rev_neg_scan_range = revert

        nx_file, nx_entry = converter.from_edf_to_nx(
            configuration=config,
        )
        hdf5_scan = HDF5TomoScan(scan=nx_file, entry=nx_entry)

        # compute expected rotation angles
        raw_rotation_angles = numpy.asarray(hdf5_scan.rotation_angle)
        converted_rotation_angles = raw_rotation_angles[
            hdf5_scan.image_key_control == 0
        ]

        if force_angles and revert and scan_range < 0 and not endpoint:
            expected_angles = numpy.linspace(0, scan_range, n_proj, endpoint=endpoint)
        else:
            expected_angles = numpy.linspace(
                min(0, scan_range), max(0, scan_range), n_proj, endpoint=endpoint
            )
            revert_angles_in_nx = force_angles and revert and (scan_range < 0)
            if revert_angles_in_nx:
                expected_angles = expected_angles[::-1]

        numpy.testing.assert_almost_equal(
            converted_rotation_angles, expected_angles, decimal=3
        )
        # test alignment projection and flat are contained in the projections range
        dark_angles = raw_rotation_angles[hdf5_scan.image_key_control == 2]
        for angle in dark_angles:
            assert angle == 0 or min(converted_rotation_angles) <= angle <= max(
                converted_rotation_angles
            )

        flat_angles = raw_rotation_angles[hdf5_scan.image_key_control == 1]

        for angle in flat_angles:
            assert angle == 0 or min(converted_rotation_angles) <= angle <= max(
                converted_rotation_angles
            )

        alignment_angles = raw_rotation_angles[hdf5_scan.image_key_control == -1]
        for angle in alignment_angles:
            assert angle == 0 or min(converted_rotation_angles) <= angle <= max(
                converted_rotation_angles
            )


def test_rot_angle_key_does_not_exists():
    """test conversion fits TomoEDFConfig parameters regarding the rotation angle calculation options"""
    with tempfile.TemporaryDirectory() as data_folder:
        scan_path = os.path.join(data_folder, "myscan")
        n_proj = 12
        n_alignment_proj = 5
        dim = 4

        MockEDF(
            scan_path=scan_path,
            n_radio=n_proj,
            n_ini_radio=n_proj,
            n_extra_radio=n_alignment_proj,
            dim=dim,
            dark_n=1,
            ref_n=1,
            scan_range=180,
        )

        output_file = os.path.join(data_folder, "nexus_file.nx")

        config = TomoEDFConfig()
        config.input_folder = scan_path
        config.output_file = output_file
        config.force_angle_calculation = False
        config.rotation_angle_keys = tuple()

        nx_file, nx_entry = converter.from_edf_to_nx(
            configuration=config,
        )
        hdf5_scan = HDF5TomoScan(scan=nx_file, entry=nx_entry)

        # compute expected rotation angles
        raw_rotation_angles = numpy.asarray(hdf5_scan.rotation_angle)
        converted_rotation_angles = raw_rotation_angles[
            hdf5_scan.image_key_control == 0
        ]

        expected_angles = numpy.linspace(
            0, 180, n_proj, endpoint=config.force_angle_calculation_endpoint
        )
        numpy.testing.assert_almost_equal(
            converted_rotation_angles, expected_angles, decimal=3
        )


def test_different_info_file():
    """insure providing a different spec info file will be taken into account"""
    with tempfile.TemporaryDirectory() as data_folder:
        scan_path = os.path.join(data_folder, "myscan")
        n_proj = 12
        n_alignment_proj = 0
        dim = 4
        flat_n = 1
        dark_n = 1

        original_scan_range = 180
        original_energy = 2.3
        original_pixel_size = 0.03
        original_distance = 0.36

        new_scan_range = -180
        new_energy = 6.6
        new_pixel_size = 0.002
        new_distance = 0.458
        new_n_proj = n_proj - 2

        other_info_file = os.path.join(data_folder, "new_info_file.info")
        dump_info_file(
            file_path=other_info_file,
            tomo_n=new_n_proj,
            scan_range=new_scan_range,
            flat_n=flat_n,
            flat_on=new_n_proj,
            dark_n=dark_n,
            dim_1=dim,
            dim_2=dim,
            col_beg=0,
            col_end=dim,
            row_beg=0,
            row_end=dim,
            pixel_size=new_pixel_size,
            distance=new_distance,
            energy=new_energy,
        )
        assert os.path.exists(other_info_file)

        MockEDF(
            scan_path=scan_path,
            n_radio=n_proj,
            n_ini_radio=n_proj,
            n_extra_radio=n_alignment_proj,
            dim=dim,
            dark_n=dark_n,
            flat_n=flat_n,
            scan_range=original_scan_range,
            energy=original_energy,
            pixel_size=original_pixel_size,
            distance=original_distance,
        )

        output_file = os.path.join(data_folder, "nexus_file.nx")
        config = TomoEDFConfig()
        config.input_folder = scan_path
        config.output_file = output_file
        config.force_angle_calculation = True
        config.pixel_size_unit = "m"
        config.distance_unit = "m"
        config.energy_unit = "kev"

        # test step 1: check scan info is correctly read from the original parameters
        nx_file, nx_entry = converter.from_edf_to_nx(
            configuration=config,
        )
        hdf5_scan_original_info_file = HDF5TomoScan(scan=nx_file, entry=nx_entry)
        assert hdf5_scan_original_info_file.energy == original_energy
        assert hdf5_scan_original_info_file.scan_range == original_scan_range
        assert hdf5_scan_original_info_file.pixel_size == original_pixel_size
        assert hdf5_scan_original_info_file.distance == original_distance
        assert len(hdf5_scan_original_info_file.projections) == n_proj

        # test step 2: check scan info is correctly read from new parameters
        config.dataset_info_file = other_info_file
        config.overwrite = True
        nx_file, nx_entry = converter.from_edf_to_nx(
            configuration=config,
        )
        hdf5_scan_new_info_file = HDF5TomoScan(scan=nx_file, entry=nx_entry)
        assert hdf5_scan_new_info_file.energy == new_energy
        # for now HDF5TomoScan expect range to be 180 or 360. There is no such -180 as in EDF
        assert abs(hdf5_scan_new_info_file.scan_range) == abs(new_scan_range)
        assert hdf5_scan_new_info_file.pixel_size == new_pixel_size
        assert hdf5_scan_new_info_file.distance == new_distance
        assert len(hdf5_scan_new_info_file.projections) == new_n_proj


def test_different_dataset_basename():
    """test conversion succeed if we provide a dataset with a different basename"""
    with tempfile.TemporaryDirectory() as data_folder:

        original_scan_path = os.path.join(data_folder, "myscan")
        new_scan_path = os.path.join(data_folder, "myscan_435")

        n_proj = 12
        n_alignment_proj = 5
        dim = 4
        energy = 12.35
        n_darks = 2
        n_flats = 1

        MockEDF(
            scan_path=original_scan_path,
            n_radio=n_proj,
            n_ini_radio=n_proj,
            n_extra_radio=n_alignment_proj,
            dim=dim,
            dark_n=n_darks,
            flat_n=n_flats,
            energy=energy,
        )

        shutil.move(
            original_scan_path,
            new_scan_path,
        )

        output_file = os.path.join(data_folder, "nexus_file.nx")

        config = TomoEDFConfig()
        config.input_folder = new_scan_path
        config.output_file = output_file
        config.dataset_basename = "myscan"
        nx_file, nx_entry = converter.from_edf_to_nx(
            configuration=config,
        )
        hdf5_scan = HDF5TomoScan(scan=nx_file, entry=nx_entry)
        assert len(hdf5_scan.projections) == n_proj
        assert len(hdf5_scan.alignment_projections) == n_alignment_proj
        assert len(hdf5_scan.darks) == n_darks
        assert len(hdf5_scan.flats) == n_flats
        assert hdf5_scan.energy == energy
