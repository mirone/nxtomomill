# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "12/03/2021"

import pytest
from nxtomomill.converter.hdf5.acquisition.baseacquisition import (
    BaseAcquisition,
    get_dataset_name_from_motor,
)
import os
from silx.io.url import DataUrl
from nxtomomill.io.config import TomoHDF5Config
import tempfile
import h5py


def test_BaseAquisition():
    """simple test of the BaseAcquisition class"""

    with tempfile.TemporaryDirectory() as folder:
        file_path = os.path.join(folder, "test.h5")
        with h5py.File(file_path, mode="w") as h5f:
            h5f["/data/toto/dataset"] = 12

        url = DataUrl(file_path=file_path, data_path="/data/toto", scheme="silx")
        std_acq = BaseAcquisition(
            root_url=url,
            configuration=TomoHDF5Config(),
            detector_sel_callback=None,
            start_index=0,
        )
        with std_acq.read_entry() as entry:
            assert "dataset" in entry


def test_get_dataset_name_from_motor():
    """test get_dataset_name_from_motor function"""
    set_1 = ["rotation", "test1", "alias"]
    assert get_dataset_name_from_motor(set_1, "rotation") == "test1"
    assert get_dataset_name_from_motor(set_1, "my motor") is None

    set_2 = ["rotation", "test1", "alias", "x translation", "m2", "test1"]
    assert get_dataset_name_from_motor(set_2, "rotation") == "test1"
    assert get_dataset_name_from_motor(set_2, "x translation") == "m2"

    set_3 = ["rotation"]
    with pytest.raises(ValueError):
        get_dataset_name_from_motor(set_3, "rotation")
