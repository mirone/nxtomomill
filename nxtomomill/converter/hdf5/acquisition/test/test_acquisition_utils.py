# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "28/04/2022"


import numpy
import pytest
from nxtomomill.converter.hdf5.acquisition.utils import deduce_machine_electric_current
from nxtomomill.utils.utils import str_datetime_to_numpy_datetime64


def test_deduce_machine_electric_current():
    """
    Test `deduce_electric_current` function. Base function to compute current for each frame according to it's timestamp
    """

    electric_current_datetimes = {
        "2022-01-15T21:07:58.360095+02:00": 1.1,
        "2022-04-15T21:07:58.360095+02:00": 121.1,
        "2022-04-15T21:09:58.360095+02:00": 123.3,
        "2022-04-15T21:11:58.360095+02:00": 523.3,
        "2022-12-15T21:07:58.360095+02:00": 1000.3,
    }
    with pytest.raises(ValueError):
        deduce_machine_electric_current(tuple(), {})
    with pytest.raises(TypeError):
        deduce_machine_electric_current(12, 2)
    with pytest.raises(TypeError):
        deduce_machine_electric_current(
            [
                12,
            ],
            2,
        )
    with pytest.raises(TypeError):
        deduce_machine_electric_current(
            [
                2,
            ],
            electric_current_datetimes,
        )

    converted_electric_currents = {}
    for elec_cur_datetime_str, elect_cur in electric_current_datetimes.items():
        datetime_as_datetime = str_datetime_to_numpy_datetime64(elec_cur_datetime_str)
        converted_electric_currents[datetime_as_datetime] = elect_cur

    # check exacts values, left and right bounds
    assert deduce_machine_electric_current(
        (str_datetime_to_numpy_datetime64("2022-01-15T21:07:58.360095+02:00"),),
        converted_electric_currents,
    ) == (1.1,)
    assert deduce_machine_electric_current(
        (str_datetime_to_numpy_datetime64("2022-01-14T21:07:58.360095+02:00"),),
        converted_electric_currents,
    ) == (1.1,)
    assert deduce_machine_electric_current(
        (str_datetime_to_numpy_datetime64("2022-12-15T21:07:58.360095+02:00"),),
        converted_electric_currents,
    ) == (1000.3,)
    assert deduce_machine_electric_current(
        (str_datetime_to_numpy_datetime64("2022-12-16T21:07:58.360095+02:00"),),
        converted_electric_currents,
    ) == (1000.3,)
    # check interpolated values
    numpy.testing.assert_almost_equal(
        deduce_machine_electric_current(
            (str_datetime_to_numpy_datetime64("2022-04-15T21:08:58.360095+02:00"),),
            converted_electric_currents,
        )[0],
        (122.2,),
    )
    numpy.testing.assert_almost_equal(
        deduce_machine_electric_current(
            (str_datetime_to_numpy_datetime64("2022-04-15T21:10:28.360095+02:00"),),
            converted_electric_currents,
        )[0],
        (223.3,),
    )

    # test several call and insure keep order
    numpy.testing.assert_almost_equal(
        deduce_machine_electric_current(
            (
                str_datetime_to_numpy_datetime64("2022-01-15T21:07:58.360095+02:00"),
                str_datetime_to_numpy_datetime64("2022-04-15T21:10:28.360095+02:00"),
                str_datetime_to_numpy_datetime64("2022-04-15T21:08:58.360095+02:00"),
            ),
            converted_electric_currents,
        ),
        (1.1, 223.3, 122.2),
    )
