# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "08/02/2021"


import os
import pytest
from nxtomomill import converter
from nxtomomill.converter.hdf5.acquisition.pcotomoacquisition import PCOTomoAcquisition
from nxtomomill.io.config import TomoHDF5Config
from nxtomomill.nexus.nxtomo import NXtomo
from nxtomomill.test.utils.bliss import MockBlissAcquisition
from nxtomomill.utils import Format
import h5py
import numpy

configs = (
    {"nb_tomo": 1, "nb_loop": 5},
    {"nb_tomo": 5, "nb_loop": 1},
    {"nb_tomo": 2, "nb_loop": 3},
)


@pytest.mark.parametrize("config", configs)
def test_pcotomo_conversion(tmp_path, config):
    n_darks = 10
    n_flats = 10
    bliss_scan_dir = str(tmp_path / "my_acquisition")
    nb_tomo = config["nb_tomo"]
    nb_loop = config["nb_loop"]
    bliss_mock = MockBlissAcquisition(
        n_sample=1,
        n_sequence=1,
        n_scan_per_sequence=1,
        n_darks=n_darks,
        n_flats=n_flats,
        output_dir=bliss_scan_dir,
        acqui_type="pcotomo",
        with_rotation_motor_info=True,
        nb_tomo=nb_tomo,
        nb_loop=nb_loop,
    )
    sample_file = bliss_mock.samples[0].sample_file

    configuration = TomoHDF5Config()
    configuration.format = Format.STANDARD

    output_nx_file = os.path.join(str(tmp_path), "nexus_scans.nx")
    assert not os.path.exists(output_nx_file)
    configuration.output_file = output_nx_file
    configuration.input_file = sample_file
    configuration.request_input = False
    configuration.raises_error = False

    result = converter.from_h5_to_nx(configuration=configuration)
    assert os.path.exists(
        output_nx_file
    )  # TODO: must create 5 files with one master file
    # check we have our 5 NXtomo converted
    assert len(result) == nb_loop * nb_tomo
    # check saved datasets
    with h5py.File(sample_file, mode="r") as h5f:
        darks = h5f["2.1/instrument/pcolinux/data"][()]
        assert len(darks) == n_darks
        flats_1 = h5f["3.1/instrument/pcolinux/data"][()]
        assert len(flats_1) == n_flats
        projections = h5f["4.1/instrument/pcolinux/data"][()]
        assert len(projections) == 10 * nb_loop * nb_tomo
        flats_2 = h5f["5.1/instrument/pcolinux/data"][()]
        assert len(flats_2) == n_flats

    for i_nx_tomo, (file_path, data_path) in enumerate(result):
        with h5py.File(file_path, mode="r") as h5f:
            detector_path = "/".join([data_path, "instrument", "detector", "data"])
            detector_data = h5f[detector_path][()]
            expected_data = numpy.concatenate(
                [
                    darks,
                    flats_1,
                    projections[(10 * i_nx_tomo) : (10 * (i_nx_tomo + 1))],
                    flats_2,
                ]
            )
            numpy.testing.assert_array_equal(detector_data, expected_data)


def test_get_projections_slices():
    """test the _get_projections_slices function"""
    nx_tomo = NXtomo("test")
    nx_tomo.instrument.detector.image_key_control = [2, 1, 0, 0, 0, 2]
    assert PCOTomoAcquisition._get_projections_slices(nx_tomo) == (slice(2, 5, 1),)
    nx_tomo.instrument.detector.image_key_control = [2, 1, 0, 0, 0, 2]
    assert PCOTomoAcquisition._get_projections_slices(nx_tomo) == (slice(2, 5, 1),)
    nx_tomo.instrument.detector.image_key_control = [2, 1]
    assert PCOTomoAcquisition._get_projections_slices(nx_tomo) == ()
    nx_tomo.instrument.detector.image_key_control = [0, 0, 2, 1, 0, 0, 1, 0, 0]
    assert PCOTomoAcquisition._get_projections_slices(nx_tomo) == (
        slice(0, 2, 1),
        slice(4, 6, 1),
        slice(7, 9, 1),
    )
