# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/02/2022"

import os
from tempfile import TemporaryDirectory
from nxtomomill.nexus.nxdetector import FieldOfView
import pytest
from nxtomomill.nexus import NXtomo
from nxtomomill.nexus import concatenate
from datetime import datetime
from tomoscan.esrf import HDF5TomoScan
from tomoscan.validator import ReconstructionValidator
from silx.io.url import DataUrl

try:
    from tomoscan.esrf.scan.hdf5scan import ImageKey
except ImportError:
    from tomoscan.esrf.hdf5scan import ImageKey
import numpy


nexus_path_versions = (1.1, 1.0, None)


@pytest.mark.parametrize("nexus_path_version", nexus_path_versions)
def test_nx_tomo(nexus_path_version):
    nx_tomo = NXtomo(node_name="")

    # check start time
    with pytest.raises(TypeError):
        nx_tomo.start_time = 12
    nx_tomo.start_time = datetime.now()

    # check end time
    with pytest.raises(TypeError):
        nx_tomo.end_time = 12
    nx_tomo.end_time = datetime(2022, 2, 27)

    # check sample
    with pytest.raises(TypeError):
        nx_tomo.sample = "tata"

    # check detector
    with pytest.raises(TypeError):
        nx_tomo.instrument.detector = "tata"

    # check energy
    with pytest.raises(TypeError):
        nx_tomo.energy = "tata"
    nx_tomo.energy = 12.3

    # check group size
    with pytest.raises(TypeError):
        nx_tomo.group_size = "tata"
    nx_tomo.group_size = 3

    # check title
    with pytest.raises(TypeError):
        nx_tomo.title = 12
    nx_tomo.title = "title"

    # check instrument
    with pytest.raises(TypeError):
        nx_tomo.instrument = "test"

    # check we can't set undefined attributes
    with pytest.raises(AttributeError):
        nx_tomo.test = 12

    # create detector for test
    projections = numpy.random.random(100 * 100 * 8).reshape([8, 100, 100])
    flats_1 = numpy.random.random(100 * 100 * 2).reshape([2, 100, 100])
    darks = numpy.random.random(100 * 100 * 3).reshape([3, 100, 100])
    flats_2 = numpy.random.random(100 * 100 * 2).reshape([2, 100, 100])
    alignment = numpy.random.random(100 * 100 * 1).reshape([1, 100, 100])
    nx_tomo.instrument.detector.data = numpy.concatenate(
        [
            darks,
            flats_1,
            projections,
            flats_2,
            alignment,
        ]
    )

    nx_tomo.instrument.detector.image_key_control = numpy.concatenate(
        [
            [ImageKey.DARK_FIELD] * 3,
            [ImageKey.FLAT_FIELD] * 2,
            [ImageKey.PROJECTION] * 8,
            [ImageKey.FLAT_FIELD] * 2,
            [ImageKey.ALIGNMENT] * 1,
        ]
    )

    nx_tomo.instrument.detector.x_pixel_size = (
        nx_tomo.instrument.detector.y_pixel_size
    ) = 1e-7
    nx_tomo.instrument.detector.distance = 0.2
    nx_tomo.instrument.detector.field_of_view = FieldOfView.HALF
    nx_tomo.instrument.detector.count_time = numpy.concatenate(
        [
            [0.2] * 3,  # darks
            [0.1] * 2,  # flats 1
            [0.1] * 8,  # projections
            [0.1] * 2,  # flats 2
            [0.1] * 1,  # alignment
        ]
    )

    # create sample for test
    nx_tomo.sample.name = "my sample"
    nx_tomo.sample.rotation_angle = numpy.concatenate(
        [
            [0.0] * 3,  # darks
            [0.0] * 2,  # flats 1
            numpy.linspace(0, 180, num=8, endpoint=False),  # projections
            [180.0] * 2,  # flats 2
            [0.0],  # alignement
        ]
    )

    n_frames = 3 + 2 + 8 + 2 + 1
    nx_tomo.sample.x_translation = [0.6] * n_frames
    nx_tomo.sample.y_translation = [0.2] * n_frames
    nx_tomo.sample.z_translation = [0.1] * n_frames
    assert nx_tomo.is_root is True
    assert nx_tomo.instrument.is_root is False
    assert (
        nx_tomo.root_path
        == nx_tomo.instrument.root_path
        == nx_tomo.instrument.detector.root_path
    )

    with TemporaryDirectory() as folder:
        file_path = os.path.join(folder, "nexus_file.hdf5")

        nx_tomo.save(
            file_path=file_path,
            data_path="entry",
            nexus_path_version=nexus_path_version,
        )
        assert os.path.exists(file_path)

        # insure we can read it from tomoscan
        scan = HDF5TomoScan(file_path, entry="entry")
        assert len(scan.flats) == 4
        assert len(scan.darks) == 3
        assert len(scan.projections) == 8
        assert len(scan.alignment_projections) == 1
        assert scan.energy == 12.3
        assert scan.pixel_size == 1e-7
        assert scan.distance == 0.2
        assert scan.field_of_view == FieldOfView.HALF
        assert scan.sample_name == "my sample"
        assert (
            len(scan.x_translation)
            == len(scan.y_translation)
            == len(scan.z_translation)
            == n_frames
        )
        assert scan.x_translation[0] == 0.6
        assert scan.y_translation[0] == 0.2
        assert scan.z_translation[0] == 0.1
        if nexus_path_version != 1.0:
            assert scan.source_name is not None
            assert scan.source_type is not None

        # check values validity using the validator
        validator = ReconstructionValidator(scan=scan)
        assert validator.is_valid()

        # try to load it from the disk
        loaded_nx_tomo = NXtomo("test").load(file_path=file_path, data_path="entry")
        assert isinstance(loaded_nx_tomo, NXtomo)
        assert loaded_nx_tomo.energy.value == nx_tomo.energy.value
        assert loaded_nx_tomo.energy.unit == nx_tomo.energy.unit
        assert loaded_nx_tomo.start_time == nx_tomo.start_time
        assert loaded_nx_tomo.end_time == nx_tomo.end_time
        assert (
            loaded_nx_tomo.instrument.detector.x_pixel_size.value
            == nx_tomo.instrument.detector.x_pixel_size.value
        )
        assert (
            loaded_nx_tomo.instrument.detector.x_pixel_size.unit
            == nx_tomo.instrument.detector.x_pixel_size.unit
        )
        assert (
            loaded_nx_tomo.instrument.detector.y_pixel_size.value
            == nx_tomo.instrument.detector.y_pixel_size.value
        )
        assert (
            loaded_nx_tomo.instrument.detector.field_of_view
            == nx_tomo.instrument.detector.field_of_view
        )
        numpy.testing.assert_array_equal(
            loaded_nx_tomo.instrument.detector.count_time.value,
            nx_tomo.instrument.detector.count_time.value,
        )
        assert (
            loaded_nx_tomo.instrument.detector.count_time.unit
            == nx_tomo.instrument.detector.count_time.unit
        )
        assert (
            loaded_nx_tomo.instrument.detector.distance.value
            == nx_tomo.instrument.detector.distance.value
        )
        assert (
            loaded_nx_tomo.instrument.detector.distance.unit
            == nx_tomo.instrument.detector.distance.unit
        )
        assert (
            loaded_nx_tomo.instrument.detector.estimated_cor_from_motor
            == nx_tomo.instrument.detector.estimated_cor_from_motor
        )

        numpy.testing.assert_array_equal(
            loaded_nx_tomo.instrument.detector.image_key_control,
            nx_tomo.instrument.detector.image_key_control,
        )
        numpy.testing.assert_array_equal(
            loaded_nx_tomo.instrument.detector.image_key,
            nx_tomo.instrument.detector.image_key,
        )

        assert loaded_nx_tomo.sample.name == nx_tomo.sample.name
        assert loaded_nx_tomo.sample.rotation_angle is not None
        numpy.testing.assert_array_almost_equal(
            loaded_nx_tomo.sample.rotation_angle, nx_tomo.sample.rotation_angle
        )
        numpy.testing.assert_array_almost_equal(
            loaded_nx_tomo.sample.x_translation.value,
            nx_tomo.sample.x_translation.value,
        )
        numpy.testing.assert_array_almost_equal(
            loaded_nx_tomo.sample.y_translation.value,
            nx_tomo.sample.y_translation.value,
        )
        numpy.testing.assert_array_almost_equal(
            loaded_nx_tomo.sample.z_translation.value,
            nx_tomo.sample.z_translation.value,
        )

        loaded_nx_tomo = NXtomo("test").load(
            file_path=file_path, data_path="entry", detector_data_as="as_numpy_array"
        )
        numpy.testing.assert_array_almost_equal(
            loaded_nx_tomo.instrument.detector.data,
            nx_tomo.instrument.detector.data,
        )
        loaded_nx_tomo = NXtomo("test").load(
            file_path=file_path, data_path="entry", detector_data_as="as_data_url"
        )
        assert isinstance(loaded_nx_tomo.instrument.detector.data[0], DataUrl)
        with pytest.raises(ValueError):
            # check an error is raise because the dataset is not virtual
            loaded_nx_tomo = NXtomo("test").load(
                file_path=file_path,
                data_path="entry",
                detector_data_as="as_virtual_source",
            )

        # test concatenation
        nx_tomo_concat = concatenate([loaded_nx_tomo, None, loaded_nx_tomo])
        concat_file = os.path.join(folder, "concatenated_nexus_file.hdf5")

        nx_tomo_concat.save(
            file_path=concat_file,
            data_path="myentry",
            nexus_path_version=nexus_path_version,
        )
        loaded_concatenated_nx_tomo = NXtomo("test").load(
            file_path=concat_file,
            data_path="myentry",
            detector_data_as="as_virtual_source",
        )
        numpy.testing.assert_array_almost_equal(
            loaded_concatenated_nx_tomo.sample.rotation_angle,
            numpy.concatenate(
                [
                    nx_tomo.sample.rotation_angle,
                    nx_tomo.sample.rotation_angle,
                ]
            ),
        )
        numpy.testing.assert_array_almost_equal(
            loaded_concatenated_nx_tomo.sample.x_translation.value,
            numpy.concatenate(
                [
                    nx_tomo.sample.x_translation.value,
                    nx_tomo.sample.x_translation.value,
                ]
            ),
        )

        with pytest.raises(TypeError):
            concatenate([1, 2])
