# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "02/05/2022"


from nxtomomill.nexus.nxmonitor import NXmonitor
from nxtomomill.nexus import concatenate
import pytest
import numpy


def test_nx_sample():
    """test creation and saving of an nxsource"""
    nx_monitor = NXmonitor()
    # check name
    with pytest.raises(TypeError):
        nx_monitor.data = 12
    with pytest.raises(ValueError):
        nx_monitor.data = numpy.zeros([12, 12])
    nx_monitor.data = tuple()
    nx_monitor.data = numpy.zeros(12)

    assert isinstance(nx_monitor.to_nx_dict(), dict)

    # test concatenate
    nx_monitor_1 = NXmonitor()
    nx_monitor_1.data = numpy.arange(10)
    nx_monitor_2 = NXmonitor()
    nx_monitor_2.data = numpy.arange(10)[::-1]
    nx_monitor_2.data.unit = "mA"

    nx_monitor_concat = concatenate([nx_monitor_1, nx_monitor_2])
    assert isinstance(nx_monitor_concat, NXmonitor)
    numpy.testing.assert_array_equal(
        nx_monitor_concat.data.value,
        numpy.concatenate(
            [
                nx_monitor_1.data.value,
                nx_monitor_2.data.value * 10e-4,
            ]
        ),
    )
