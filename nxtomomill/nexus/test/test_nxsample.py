# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/02/2022"


from nxtomomill.nexus.nxsample import NXsample
import pytest
import numpy


def test_nx_sample():
    """test creation and saving of an nxsource"""
    nx_sample = NXsample()
    # check name
    with pytest.raises(TypeError):
        nx_sample.name = 12
    nx_sample.name = "my sample"

    # check rotation angle
    with pytest.raises(TypeError):
        nx_sample.rotation_angle = 56
    nx_sample.rotation_angle = numpy.linspace(0, 180, 180, endpoint=False)

    # check x translation
    with pytest.raises(TypeError):
        nx_sample.x_translation = 56
    nx_sample.x_translation = numpy.linspace(0, 180, 180, endpoint=False)

    # check y translation
    with pytest.raises(TypeError):
        nx_sample.y_translation = 56
    nx_sample.y_translation = [0.0] * 180

    # check z translation
    with pytest.raises(TypeError):
        nx_sample.x_translation = 56
    nx_sample.z_translation = None

    assert isinstance(nx_sample.to_nx_dict(), dict)

    # check we can't set undefined attributes
    with pytest.raises(AttributeError):
        nx_sample.test = 12

    # test concatenation
    nx_sample_concat = NXsample.concatenate([nx_sample, nx_sample])
    assert nx_sample_concat.name == "my sample"
    numpy.testing.assert_array_equal(
        nx_sample_concat.rotation_angle,
        numpy.concatenate(
            [
                numpy.linspace(0, 180, 180, endpoint=False),
                numpy.linspace(0, 180, 180, endpoint=False),
            ]
        ),
    )

    numpy.testing.assert_array_equal(
        nx_sample_concat.x_translation.value,
        numpy.concatenate(
            [
                numpy.linspace(0, 180, 180, endpoint=False),
                numpy.linspace(0, 180, 180, endpoint=False),
            ]
        ),
    )

    numpy.testing.assert_array_equal(
        nx_sample_concat.y_translation.value,
        numpy.concatenate(
            [
                numpy.asarray([0.0] * 180),
                numpy.asarray([0.0] * 180),
            ]
        ),
    )

    assert nx_sample_concat.z_translation.value is None
