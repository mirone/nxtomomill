# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "10/02/2022"


from nxtomomill.nexus.nxdetector import NXdetector
from nxtomomill.nexus.nxinstrument import NXinstrument
from nxtomomill.nexus.nxsource import NXsource, DefaultESRFSource
import pytest


def test_nx_instrument():
    """test creation and saving of an nxinstrument"""
    nx_instrument = NXinstrument()

    # check data
    with pytest.raises(TypeError):
        nx_instrument.detector = 12
    nx_instrument.detector = NXdetector(node_name="test")

    with pytest.raises(TypeError):
        nx_instrument.diode = 12
    nx_instrument.diode = NXdetector(node_name="test 2")

    with pytest.raises(TypeError):
        nx_instrument.source = 12
    nx_instrument.source = DefaultESRFSource()

    with pytest.raises(TypeError):
        nx_instrument.diode = NXsource(node_name="my source")
    nx_instrument.diode = NXdetector(node_name="det34")

    assert isinstance(nx_instrument.to_nx_dict(), dict)

    with pytest.raises(TypeError):
        nx_instrument.name = 12
    nx_instrument.name = "test name"
    assert nx_instrument.name == "test name"

    # check we can't set undefined attributes
    with pytest.raises(AttributeError):
        nx_instrument.test = 12

    # test concatenation
    nx_instrument_concat = NXinstrument.concatenate([nx_instrument, nx_instrument])
    assert nx_instrument_concat.name == "test name"
