# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2016-2022 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/


__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "04/02/2022"


from nxtomomill.nexus.nxsource import NXsource
import pytest


def test_nx_source():
    """test creation and saving of an nxsource"""
    nx_source = NXsource()
    with pytest.raises(TypeError):
        nx_source.name = 12
    nx_source.name = "my source"

    with pytest.raises(AttributeError):
        nx_source.source_name = "test"

    with pytest.raises(ValueError):
        nx_source.type = "toto"
    nx_source.type = "Synchrotron X-ray Source"
    str(nx_source)
    nx_source.type = None
    str(nx_source)

    assert isinstance(nx_source.to_nx_dict(), dict)

    # check we can't set undefined attributes
    with pytest.raises(AttributeError):
        nx_source.test = 12

    # test some concatenation
    nx_source_concatenate = NXsource.concatenate([nx_source, nx_source])
    assert nx_source_concatenate.name == "my source"
    assert nx_source_concatenate.type is None
