# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2020 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

"""
test the patch-nx application
"""

__authors__ = [
    "H. Payno",
]
__license__ = "MIT"
__date__ = "30/08/2021"


import tempfile
from tomoscan.esrf.mock import MockHDF5
from nxtomomill.app.patch_nx import main
import os


def test_patch_nx_application():
    """test nxtomomill patch-nx input_file entry --invalid-frames XX:YY"""

    with tempfile.TemporaryDirectory() as folder:
        nx_path = os.path.join(folder, "nexus_file.nx")
        dim = 55
        nproj = 20
        scan = MockHDF5(
            scan_path=nx_path,
            n_proj=nproj,
            n_ini_proj=nproj,
            create_ini_dark=False,
            create_ini_flat=False,
            create_final_flat=False,
            dim=dim,
        ).scan
        main(
            [
                "patch-nx",
                scan.master_file,
                "entry",
                "--invalid-frames",
                "0:12",
            ]
        )
        scan.clear_caches()
        assert len(scan.projections) == 8, "Scan is expected to have 8 projections now"
