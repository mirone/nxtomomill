# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2020 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

"""
test the h52nx application
"""

__authors__ = [
    "H. Payno",
]
__license__ = "MIT"
__date__ = "30/08/2021"


import tempfile
from nxtomomill.test.utils.bliss import MockBlissAcquisition
from nxtomomill.app.h52nx import main
import os


def test_h52nx_application():
    """test nxtomomill h52nx input_file output_file --single-file"""
    with tempfile.TemporaryDirectory() as folder:
        nx_file_path = os.path.join(folder, "acquisition.nx")

        bliss_mock = MockBlissAcquisition(
            n_sample=2,
            n_sequence=1,
            n_scan_per_sequence=10,
            n_darks=5,
            n_flats=5,
            with_nx_detector_attr=True,
            output_dir=folder,
            detector_name="pcolinux",
        )

        h5_file_path = bliss_mock.samples[0].sample_file
        assert not os.path.exists(nx_file_path), "outputfile exists already"
        main(["h52nx", h5_file_path, nx_file_path, "--single-file"])
        assert os.path.exists(nx_file_path), "outputfile doesn't exists"
