# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2020 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

"""
test the edf2nx application
"""

__authors__ = [
    "H. Payno",
]
__license__ = "MIT"
__date__ = "30/08/2021"


import tempfile
from tomoscan.esrf.mock import MockEDF
from nxtomomill.app.edf2nx import main
import os


def test_edf2nx_application():
    """test nxtomomill edf2nx input_file output_file"""
    with tempfile.TemporaryDirectory() as folder:
        edf_acq_path = os.path.join(folder, "acquisition")
        n_proj = 10
        MockEDF(
            scan_path=edf_acq_path,
            n_radio=n_proj,
            n_ini_radio=n_proj,
        )
        output_file = os.path.join(edf_acq_path, "nexus_file.nx")

        assert not os.path.exists(output_file), "output_file exists already"
        main(["edf2nx", edf_acq_path, output_file])
        assert os.path.exists(output_file), "output_file doesn't exists"
