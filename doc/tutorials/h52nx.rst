.. _Tomoh52nx:

h52nx tutorial
==============

the `h52nx` application can be used to convert acquisition from hdf5/bliss to a nexus - NXtomo compliant format.
This format will also be stored in .h5 / .hdf5 / .nx file.
For comprehension we will use the nexus format (.nx) in this tutorial.

To call this application you can call directly

.. code-block:: bash

   $ nxtomomill h52nx [options]

if nxtomomill has been installed in the global scope.
Otherwise you can call


.. code-block:: bash

   $ python -m nxtomomill h52nx [options]

Then you can convert bliss h5 file :ref:`h52nx_without_config_file` or :ref:`h52nx_with_config_file`

.. _h52nx_without_config_file:

without configuration file
--------------------------

The first two parameters should be:

* input-file-name: bliss.hdf5 master file, containing the details of the acquisition
* output-file: destination file where result will be store.

Sor for example to convert a file name 'bliss.hdf5' to a 'my_nxtomo_file.nx' you should call:


.. code-block:: bash

   $ python -m nxtomomill h52nx bliss.hdf5 my_nxtomo_file.nx


You can also access the help of h52nx by calling:


.. code-block:: bash

   $ python -m nxtomomill h52nx --help


By default if some information are missing the converter will ask you for missing input (it can be the case of the incoming energy for example).
If you want to avoid converter for information you can add the `--no-input` option.

An acquisition file can contain several sequence (so several acquisition).
`h52nx` will convert them all create one file per acquisition and one file referencing all acquisitions.
You can ask the converter to keep all the acquisition into a single file using the '--single-file' option.


Input type
^^^^^^^^^^

z-series
""""""""

z-series are handled by nxtomomill since 0.4. It will create one entry per 'z' found.

XRD-CT
""""""

XRD-CT data is handle since 0.5. nxtomomill try to deduce the type from parsing the input file. It can fail sometime.
You can enforce the the input type by using the "--xrd-ct-format" option from the command line.

For XRD-CT we have a 'diode' dataset that does not appear for other input type.

Settings
^^^^^^^^

The `h5tonx` command is using the 'h5_to_nx' function from 'converter' module.
This will used by default :ref:`Settings`  defined in nxtomomill.settings.py file
If you want you can overwrite them from the command line.

camera name
"""""""""""

In order to know if an NXDetector should be converted or not it relies on 'H5_VALID_CAMERA_NAMES' defined in nxtomomill.settings.py.
 * If the value is None (default) then we will first try to retrieve group (under instrument) that are defined as an 'NXdetector' NX_class. If none are found then we try to retrieve group that 'looks' like a detector (containing a dataset name 'data' and which is of dimension 3).
 * It can also be set as a tuple of string. Each string is a detector name to be handle. Those can handle Linux wildcard (like 'frelon*')
 * the values defined in settings can be overwrite by `--valid_camera_names` option (see help).


other (rotation angle, translation...)
""""""""""""""""""""""""""""""""""""""

Most of the key used to retrieve other operation can be overwrite from
an option from command line

.. code-block:: bash

  $ nxtomomill h52nx --help
  convert data acquired as hdf5 from bliss to nexus `NXtomo` classes. For
  `zseries` it will create one entry per `z`

  positional arguments:
    input_file            master file of the acquisition
    output_file           output .nx or .h5 file

  optional arguments:
    -h, --help            show this help message and exit
    --file_extension FILE_EXTENSION
                          extension of the output file. Valid values are
                          .h5/.hdf5/.nx
    --single-file         merge all scan sequence to the same output file. By
                          default create one file per sequence and group all
                          sequence in the output file
    --overwrite           Do not ask for user permission to overwrite output
                          files
    --debug               Set logs to debug mode
    --entries ENTRIES     Specify (root) entries to be converted. By default it
                          will try to convert all existing entries.
    --ignore-sub-entries IGNORE_SUB_ENTRIES
                          Specify (none-root) sub entries to ignore.
    --raises-error        Raise errors if some data are not met instead of
                          providing some default values
    --no-input, --no-input-for-missing-information
                          The user won't be ask for any inputs
    --standard-format     Format of the input file is the 'standard' tomography
                          format
    --xrd-ct-format       Format of the input file is the 'XRD-CT' tomography
                          format
    --x_trans_keys X_TRANS_KEYS, --x-trans-keys X_TRANS_KEYS
                          x translation key in bliss HDF5 file
    --y_trans_keys Y_TRANS_KEYS, --y-trans-keys Y_TRANS_KEYS
                          y translation key in bliss HDF5 file
    --z_trans_keys Z_TRANS_KEYS, --z-trans-keys Z_TRANS_KEYS
                          z translation key in bliss HDF5 file
    --valid_camera_names VALID_CAMERA_NAMES, --valid-camera-names VALID_CAMERA_NAMES
                          Valid NXDetector dataset name to be considered.
                          Otherwise willtry to deduce them from NX_class
                          attibute (value should beNXdetector) or from
                          instrument group child structure.
    --rot_angle_keys ROT_ANGLE_KEYS, --rot-angle-keys ROT_ANGLE_KEYS
                          Valid dataset name for rotation angle
    --acq_expo_time_keys ACQ_EXPO_TIME_KEYS, --acq-expo-time-keys ACQ_EXPO_TIME_KEYS
                          Valid dataset name for acquisition exposure time
    --x_pixel_size_key X_PIXEL_SIZE_KEY, --x-pixel-size-key X_PIXEL_SIZE_KEY
                          X pixel size key to read
    --y_pixel_size_key Y_PIXEL_SIZE_KEY, --y-pixel-size-key Y_PIXEL_SIZE_KEY
                          Y pixel size key to read
    --init_titles INIT_TITLES, --init-titles INIT_TITLES
                          Titles corresponding to init scans
    --init_zserie_titles INIT_ZSERIE_TITLES, --init-zserie-titles INIT_ZSERIE_TITLES
                          Titles corresponding to zserie init scans
    --dark_titles DARK_TITLES, --dark-titles DARK_TITLES
                          Titles corresponding to dark scans
    --flat_titles --flat_titles --ref_titles FLAT_TITLES, --ref-titles FLAT_TITLES
                          Titles corresponding to ref scans
    --proj_titles PROJ_TITLES, --proj-titles PROJ_TITLES
                          Titles corresponding to projection scans
    --align_titles ALIGN_TITLES, --align-titles ALIGN_TITLES
                          Titles corresponding to alignment scans
    --set-params [SET_PARAMS [SET_PARAMS ...]]
                          Allow manual definition of some parameters. Valid
                          parameters (and expected input unit) are: energy
                          (kev), x_pixel_size (m), y_pixel_size (m). Should be
                          added at the end of the command line because will try
                          to cover all text set after this option.
    --config CONFIG, --config-file CONFIG, --configuration CONFIG, --configuration-file CONFIG
                          file containing the full configuration to convert from
                          h5 bliss to nexus

.. _h52nx_with_config_file:

with a configuration file
--------------------------

Generate a default configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can create a default configuration file by calling

.. code-block:: bash

    $ nxtomomill h5-quick-start [output_conf_file.cfg]

This will generate a configuration file based on the existing :ref:`Settings` that you can edit.

Once the configuration file fit your needs you can execute it by calling h52nx.

.. code-block:: bash

    $ nxtomomill h52nx input_bliss_file.h5 [output_nexus_file.nx] --config [output_conf_file.cfg]

.. warning::

    when you provide a configuration file to h52nx then no other input can be provided (excepted for the output file which is optional).

.. note:: If you want to provide configuration from scan titles you can ignore the `FRAME_TYPE_SECTION` and generate a default configuration file with the option `--from-title-names`.
          On the contrary if you want to ignore the `ENTRIES_AND_TITLES_SECTION` you can use the `--from-scan-urls` option.

Example of a configuration file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: resources/default.cfg
   :literal:


.. warning:: the "data_scans" values from the `FRAME_TYPE_SECTION` section should be provided with indentation (at least one empty space. Otherwise they will be ignored by configparser.
