.. _dxfile2nxtutorial:

dxfile2nx tutorial
==================

the `dxfile2nx` application is used to convert an acquisition stored with the `dxfile format <https://dxfile.readthedocs.io/en/latest/>`_ to the `NXTomo <https://manual.nexusformat.org/classes/applications/NXtomo.html>`_ format.

To call this application you can call directly

.. code-block:: bash

   nxtomomill dxfile2nx [input_file] [output_file] [[options]]

You can access help by calling

.. code-block:: bash

    $ nxtomomill dxfile2nx --help
    usage: nxtomomill dxfile2nx [-h] [--input_entry INPUT_ENTRY]
                                [--output_entry OUTPUT_ENTRY]
                                [--file_extension FILE_EXTENSION]
                                [--scan-range SCAN_RANGE]
                                [--pixel-size PIXEL_SIZE] [--fov FIELD_OF_VIEW]
                                [--distance DISTANCE] [--energy ENERGY]
                                [--overwrite] [--no-data-copy]
                                input_file output_file

    convert acquisition contained in provided dx file to NXTomo entry

    positional arguments:
      input_file            master file of the acquisition
      output_file           output .nx or .h5 file

    optional arguments:
      -h, --help            show this help message and exit
      --input_entry INPUT_ENTRY
                            h5py group path to be converted
      --output_entry OUTPUT_ENTRY
                            h5py group path to store the NXTomo
      --file_extension FILE_EXTENSION
                            extension of the output file. Valid values are
                            .h5/.hdf5/.nx
      --scan-range SCAN_RANGE
                            scan range of the projections. Dark and flat will
                            always be considered with rotation angle ==
                            scan_range[0]
      --pixel-size PIXEL_SIZE
                            pixel size in meter as (x pixel size, y pixel size)
      --fov FIELD_OF_VIEW, --field-of-view FIELD_OF_VIEW
                            pixel size in meter as (x pixel size, y pixel size)
      --distance DISTANCE, --detector-sample-distance DISTANCE
                            sample to detector distance (in meter)
      --energy ENERGY, --incident-beam-energy ENERGY
                            incident beam energy in keV
      --overwrite           Do not ask for user permission to overwrite output
                            files
      --no-data-copy        Avoid creating any symbolic link between input and
                            output fileand duplicate the frame datasets to have a
                            'all-in-one' output file.

Here is an example on how to convert a dxfile.
For it we downloaded the `tomo_00068 dataset <https://tomobank.readthedocs.io/en/latest/source/data/docs.data.phasecontrast.html#wet-sample>`_

To convert it we simply go for

.. code-block:: bash

    nxtomomill dxfile2nx tomo_00068.h5 tomo_00068.nx

Here the conversion will set default values to parameters that are not recorded in the dxfile but that tomography software mights need. Such as beam energy ...
Here is an example of providing more information to the converter and requesting the converter to overwrite the output NXtomo entry if it exists

.. code-block:: bash

    nxtomomill dxfile2nx tomo_00068.h5 tomo_00068.nx --scan-range 0,180 --distance 0.15 --energy 14 --overwrite

Here is an example of the NXTomo entry generated:

.. image:: img/tomo_00068_nxtomo_entry.png
   :width: 600 px
   :align: center

From it you can can several tomography tool like `nabu <https://gitlab.esrf.fr/tomotools/nabu>`_ or `nabu <https://gitlab.esrf.fr/tomotools/tomwer>`_.

.. warning:: to have a complete usage of tomwer you will also have to provide the field of view of the detector (Full or Half)

Example of the reconstruction of the middle slice using `nabu <https://gitlab.esrf.fr/tomotools/nabu>`_:

.. image:: img/tomo_00068_nabu_rec.png
   :width: 600 px
   :align: center
