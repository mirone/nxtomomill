.. _edf2nxtutorial:

edf2nx tutorial
===============


the `edf2nx` application is used to convert acquisition from edf standard tomography scans to a nexus/hdf5 - NXtomo compliant format.
This format will also be stored in .h5 / .hdf5 / .nx file.
For comprehension we will use the nexus format (.nx) in this tutorial.

To access this application you can call directly

.. code-block:: bash

   nxtomomill edf2nx [-h] [--dataset-basename DATASET_BASENAME] [--info-file INFO_FILE] [--config CONFIG] [scan_path] [output_file]

if nxtomomill has been installed in the global scope.
Otherwise you can call


.. code-block:: bash

   nxtomomill edf2nx [options]


simple convertion (no configuration file)
-----------------------------------------

To execute a conversion from EDF-Spec to NXtomo using the default parameters the first two parameters should be:

* input folder directory aka scan_path: root directory containing all the .edf frames and the .info file of the acquisition.
  By default it expects this folder name to be the .edf file prefix (as `folder_name_0000.edf...`) and .info file to be named `folder_name.info`.
  To have advanced option on this please have a look at :ref:`edf2nxtutorialConfigFile` and on `dataset_basename` and `dataset_info_file` fields.
* output_file: output filename which will contain the NXtomo created

Sor for example to convert an edf-like tomography dataset '/data/idxx/inhouse/myname/sample1_' to 'sample1_.nx' you should call:


.. code-block:: bash

   nxtomomill edf2nx /data/idxx/inhouse/myname/sample1_  /data/idxx/inhouse/myname/sample1_.nx


Normally the resulting file should have more or less the same size than the initial directory.


You can also access the help of edf2nx by calling:


.. code-block:: bash

   nxtomomill edf2nx --help


The result can be displayed using any hdf5 display or using silx:

.. code-block:: bash

   silx view /data/idxx/inhouse/myname/sample1_.nx

All the .edf files in the origin directory are considered except those having '_slice_' in their name.

The algorithm selects raw dark fields - darkendxxxx.edf - and raw flat fields refxxxx.edf. However, if processed darks (dark.edf) and refs (refHST) exist,
they are stored in the destination file instead of the raw files.
The names of the motors are defined to some defauls ('srot', 'somega') for the rotation, 'sx', 'sy' and 'sz' for the positioning motors.
You can defined different keys from the configuration file. If you have 'redundant' keys to be used you can also let us know so we can add those as default keys.


.. edf2nxtutorialConfigFile:


advanced convertion (using configuration file)
----------------------------------------------

The conversion is based on settings (defined in settings.py module and EDFTomoConfig class). In order to define:

1. which key of the EDF headers should be used to get rotation angle, translations
2. prefix to be used to deduce dark and flat files
3. rules to compute rotation angle if we cannot deduce them from edf headers
4. pattern to recognize some file to be ignored (like pyhst reconstruction files)

All of the settings used can be defined on a configuration file. A configuration file can be created from:

.. code-block:: bash

   nxtomomill edf-quick-start [edf_2nx_config.cfg] [--level]

For now user can get configuration file with two level of details: `required` and `advanced`.

Sections and fields comments should be clear enought for you to understand the meaning of each elements (otherwise let us know).
We can notice that for the previous explained case:

1. define EDF header keys to be used: this will be defined in the *EDF_KEYS_SECTION* section
2. define prefix to be used for dark and flat: this will be defined in the *DARK_AND_FLAT_SECTION* section
3. rules to compute rotation angle if we cannot deduce them from edf headers: this will be defined in the *SAMPLE_SECTION* section
4. pattern to recognize some file to be ignored (like pyhst reconstruction files): this will be defined in the *GENERAL_SECTION* section

Regarding the *GENERAL_SECTION* we can also provide more hint on:

* `dataset_basename`: the `dataset_basename` will be used to deduce all the information required to build a NXtomo: get projections files (like dataset_basename_XXXX.edf) and retrieve infomration like energy, sample / detector distance from the >info file.
                      If not provided then the `dataset_basename` will be the name of the provided folder.
* `dataset_info_file`: In order to retrieve scan range, energy, distance, pixel size... we use a .info file with predefined keys.
                       If not provided the converted will look for a `dataset_basename.info` file. But you can provide provide path to another .info file to be used.


Once your configuration is edited you can use it from the `nxtomomill edf2nx` using the `--config` option like:

.. code-block:: bash

   nxtomomill edf2nx --config edf_2nx_config.cfg

.. note:: to ease usage the "dataset basename" and the "info file" can also be provided from command line (`--dataset-basename` and `--info-file` options).
          If you provide one of those parameters from the command line option then it should not be provided from the configuration.
          This is the same for `scan_path` aka folder path (containing raw data / .edf) and `output_file`
