[dataset]
# Dataset location, either a directory or a HDF5-Nexus file.
location = nxtomo.nx


[preproc]
# How to perform flat-field normalization. The parameter value can be:
#  - 1 or True: enabled.
#  - 0 or False: disabled
#  - forced or force-load: perform flatfield regardless of the dataset by attempting to load darks/flats from 'xxx_tomwer_processes.h5' file or 'xxx_nabu_processes.h5' or the file provided in parameter 'processes_file'
#  - force-compute: perform flatfield, ignore all .h5 files containing already computed darks/flats.
flatfield = 0
# Whether to enable the CCD hotspots correction.
ccd_filter_enabled = 0
# If ccd_filter_enabled = 1, a median filter is applied on the 3X3 neighborhood
# of every pixel. If a pixel value exceeds the median value more than this parameter,
# then the pixel value is replaced with the median value.
ccd_filter_threshold = 0.04
# Whether to enable the 'double flat-field' filetering for correcting rings artefacts.
double_flatfield_enabled = 0
# Whether to take logarithm after flat-field and phase retrieval.
take_logarithm = 0
# Sinogram rings removal method. Default (empty) is None. Available are: None, munch. See also: sino_rings_options
sino_rings_correction = 


[phase]
# Phase retrieval method. Available are: Paganin, CTF, None
method = none
# Single-distance phase retrieval related parameters
# ----------------------------
# delta/beta ratio for the Paganin/CTF method
delta_beta = 100.0
# Unsharp mask strength. The unsharped image is equal to
#   UnsharpedImage =  (1 + coeff)*originalPaganinImage - coeff * ConvolvedImage. Setting this coefficient to zero means that no unsharp mask will be applied.
unsharp_coeff = 0
# Standard deviation of the Gaussian filter when applying an unsharp mask
# after the phase filtering. Disabled if set to 0.
unsharp_sigma = 0
# Which type of unsharp mask filter to use. Available values are gaussian (UnsharpedImage =  (1 + coeff)*originalPaganinImage - coeff * ConvolvedImage) and laplacian (UnsharpedImage = originalPaganinImage + coeff * ConvolvedImage). Default is gaussian.
unsharp_method = gaussian
# Geometric parameters for CTF phase retrieval. Length units are in meters.
ctf_geometry = z1_v=None; z1_h=None; detec_pixel_size=None; magnification=True


[reconstruction]
# Reconstruction method. Possible values: FBP, none. If value is 'none', no reconstruction will be done.
method = FBP
# In the case you want to override the angles found in the files metadata. The angles are in degree.
angles_file = 
# Rotation axis position. Default (empty) is the middle of the detector width.
# Additonally, the following methods are available to find automaticall the Center of Rotation (CoR):
#  - centered : a fast and simple auto-CoR method. It only works when the CoR is not far from the middle of the detector. It does not work for half-tomography.
#  - global : a slow but robust auto-CoR.
#  - sliding-window : semi-automatically find the CoR with a sliding window. You have to specify on which side the CoR is (left, center, right). Please see the 'cor_options' parameter.
#  - growing-window : automatically find the CoR with a sliding-and-growing window. You can tune the option with the parameter 'cor_options'.
#  - sino-coarse-to-fine: Estimate CoR from sinogram. Only works for 360 degrees scans.
#  - composite-coarse-to-fine: Estimate CoR from composite multi-angle images. Only works for 360 degrees scans.
rotation_axis_position = 
# Padding type for FBP. Available are: zeros, edges
padding_type = zeros
# Whether to enable half-acquisition. Default is auto. You can enable/disable it manually by setting 1 or 0.
enable_halftomo = auto
# Whether to set to zero voxels falling outside of the reconstruction region
clip_outer_circle = 0
# If set to true, the reconstructed region is centered on the rotation axis, i.e the center of the image will be the rotation axis position.
centered_axis = 0

# Parameters for sub-volume reconstruction. Indices start at 0 !
# ----------------------------------------------------------------
# (x, y) are the dimension of a slice, and (z) is the 'vertical' axis
# By default, all the volume is reconstructed slice by slice, along the axis 'z'.
start_x = 0
end_x = -1
start_y = 0
end_y = -1
start_z = 0
end_z = 1


[output]
# Directory where the output reconstruction is stored.
location = ./
# File prefix. Optional, by default it is inferred from the scanned dataset.
file_prefix = 
# Output file format. Available are: hdf5, tiff, jp2, edf
file_format = hdf5
# What to do in the case where the output file exists.
# By default, the output data is never overwritten and the process is interrupted if the file already exists.
# Set this option to 1 if you want to overwrite the output files.
overwrite_results = 1


[postproc]
# Whether to compute a histogram of the volume.
output_histogram = 0


[resources]
# Computations distribution method. It can be:
#   - local:  run the computations on the local machine
#   - slurm: run the computations through SLURM
#   - preview: reconstruct the slices/volume as quickly as possible, possibly doing some binning.
method = local


[pipeline]
# Save intermediate results. This is a list of comma-separated processing steps, for ex: flatfield, phase, sinogram.
# Each step generates a HDF5 file in the form name_file_prefix.hdf5 (ex. 'sinogram_file_prefix.hdf5')
save_steps = 
# Resume the processing from a previously saved processing step. The corresponding file must exist in the output directory.
resume_from_step = 
# Level of verbosity of the processing. 0 = terse, 3 = much information.
verbosity = 2


[about]
