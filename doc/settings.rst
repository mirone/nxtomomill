Settings
========

nxtomomill needs to get several information in order to complete translation from EDF or (bliss) HDF5 to nexus-compliant HDF5 file.

This information
   * is a set of key values for EDF
   * is a set of dataset location and name for (bliss) HDF5

All are stored in nxtomomill.settings.py file. For `tomoh52nx` you can generally overwrite from calling the appropriate command option  (see :ref:`Tomoh52nx`).

But for scripting are when used from a dependency (like tomwer) it can be convenient to modify some of them.
For example if you cannot retrieve rotation angle because this information is stored at a different location than the 'default' one knows by nxtomomill you can change `H5_Y_ROT_KEY` parameter.

If one of the requested information is a combination of dataset value then you light probably want to add a plugin for it :ref:`Plugins`
