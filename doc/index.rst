.. nxtomomill documentation

.. |foo| image:: img/nxtomomill.png
    :scale: 22 %


============================================
|foo| Welcome to nxtomomill's documentation!
============================================

nxtomomill provide a set of applications to convert tomography acquisition made by `BLISS <https://www.esrf.fr/BLISS>`_ from their original file format (.edf, .h5) to a Nexus compliant file format (using `NXtomo <https://manual.nexusformat.org/classes/applications/NXtomo.html>`_)

.. include:: install.rst

Tutorials
=========

.. toctree::
   :maxdepth: 2

   tutorials/index.rst

API
===

.. toctree::
   :maxdepth: 4

   api.rst


.. include:: plugins.rst

.. include:: settings.rst

.. include:: design/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
