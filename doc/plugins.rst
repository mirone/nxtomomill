.. _Plugins:

Plugins
=======

HDF5Plugin
""""""""""

In some cases you might want to modify information contained on an NXtomo. For example if you want to do some operations between raw dataset or append some information that does not appears on the raw file.

This is the goal of the 'plugin mecanism'.

All the plugins will be discover during the bliss-hdf5 to nexus_hdf5 conversion and will be apply just before saving NXtomo(s) to disk.

In order to create a plugin you must:

- create a directory that will contain the plugins.
- define the environment variable 'NXTOMOMILL_PLUGINS_DIR' which register the plugin directory
- inside the plugin directory create one or several python file which will implement the 'HDF5Plugin' class.

You can retrieve data from bliss-hdf5 acquisition contained either in `instrument` or in `positioner` groups.

For performance issues, as we collect this information in the NXtomo creation preprocessing you must declare `dataset` name you want to retrieve from those groups.

The HDF5Plugin provide an API to modify the NXtomo before they will be saved on disk. This way you can modify them as you like using the NXtomo API by implementing the `update_nx_tomo` funciton.

.. code-block:: python

    from nxtomomill.plugins import HDF5Plugin
    import numpy


    class MyPlugin1(HDF5Plugin):
        """

        :warning: the constructor should not take any parameter because
        instantiate by nxtomomill
        """
        def __init__(self):
            positioners_names = 'diffty', 'd1ty'
            super().__init__(name='My plugin 1', positioner_keys=positioners_names)

        def update_nx_tomo(self, nx_tomo) -> nx_tomo:
            diffty = numpy.array(self.get_positioner_info('diffty'))
            d1ty = numpy.array(self.get_positioner_info('d1ty'))

            nx_tomo.sample.x_translation = diffty - d1ty - 14
            nx_tomo.sample.x_translation.unit = "mm"
