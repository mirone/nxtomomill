Installation
============

The simplest way to install nxtomomill is to use the pypi wheel provided ()

.. code-block:: bash

   pip install nxtomomill


You can also install it from `source <https://gitlab.esrf.fr/tomotools/nxtomomill>`_:


.. code-block:: bash

   git clone https://gitlab.esrf.fr/tomotools/nxtomomill.git
   cd nxtomomill
   pip install .


Building doc
''''''''''''

In order to build documentation you should insure `sphinx <https://www.sphinx-doc.org/>`_ is install.
You can for example install it from pip:

.. code-block:: bash

    pip install sphinx

Then you can build the documentation using sphinx:

.. code-block:: bash

    python setup.py build build_sphinx
