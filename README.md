# nxtomomill

nxtomomill provide a set of applications and tools around the [NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html) format defined by the [NeXus community](https://manual.nexusformat.org/index.html#).

It includes for example the convertion from bliss raw data (@ESRF) to NXtomo, or from spec EDF (@ESRF) to NXtomo. But also creation from scratch and edition of an NXtomo from a python API.

## installation

To install the latest 'nxtomomill' pip package

```bash
pip install nxtomomill
```

You can also install nxtomomill from source: 

```bash
pip install git+https://gitlab.esrf.fr/tomotools/nxtomomill.git
```

## documentation

General documentation can be found here: [https://tomotools.gitlab-pages.esrf.fr/nxtomomill/](https://tomotools.gitlab-pages.esrf.fr/nxtomomill/)

## application

documentation regarding applications can be found here: [https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/index.html](https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/index.html)

or to get help you can directly go for

```bash
nxtomomill --help
```
