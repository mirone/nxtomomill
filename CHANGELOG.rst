Change Log
==========

0.8.0: 2021/06/04
-----------------

* converter
    * hdf5
        * add 'bam_single_file' option (PR 96)
        * add pcotomo management (PR 91, 88)

* add nexus module providing API to edit an NXtomo (PR 87)


0.7.0: 2021/01/07
-----------------

* converter
    * hdf5: management of ExternalLink as entry (case of bliss proposal file, PR 85)
    * edf
        * fix issues with progress (PR 80)

* patch-nx
    * add option to convert all frame of a given type (PR 84)

* dxfile2nx
    * allow to provide pixel size as a single value (PR 77)

* miscellaneous::
    * add some missing aliases flat/ref (PR 83)
    * benefit from validator (PR 81)


0.6.0: 2021/10/04
-----------------

* app
    * add 'dxfile2nx' application. Allow to convert from dxfile format to NXtomo format
    * h52nx
        * add "duplicate_data" option to force frame duplication from the command line.
    * add 'h5-3dxrd-2nx': convert from to 3D XRD - bliss-hdf5 to (enhance) NXtomo Format .

* converter
    * add dxfileconverter


0.5.0: 2021/04/20
-----------------

* converter
    * hdf5
        * rework virtual dataset creation
        * insure the number of projection found is that same as the expected number of projection (tomo_n)
        * add `start_time` and `end_time`
        * handle configuration file
            * add `HDF5config` and `HDF5ConfigHandler`
        * improve research of keys / path. First try to retrieve a dataset with the expected number of elements. If fail return the first dataset fitting a path.
        * add `ignore_sub_entries` to skip some scan
        * add a warning if no acquisition are found
    * utils
        * rework `_insert_frame_data` to use a class instead `_FrameAppender`
        * fix management of negative indexing
* app
    * patch-nx
        * add an option `--embed-data`
    * rename `tomoh52nx` to `h52nx`. Deprecate `tomoh52nx`.
    * rename `tomoedf2nx` to `edf2nx`. Deprecate `tomoedf2nx`.
    * `h52nx`
        * can now take a configuration file in parameter ("--config" option)
        * add option `--ignore-sub-entries`


0.4.0: 2020/11/09
-----------------

* requires h5py >= 3
* utils:
    * add `change_image_key_control` function to modify frame type inplace
    * add `add_dark_flat_nx_file` function to add dark or flat in an existing `NXTomo` entry
    * converter
        * h5_to_nx:
            * add management of 'proposal file': handle External / SoftLink
            * insure relative path is given when converting the file
            * magnified_pixel_size will not be write anymore. If a magnified / sample pixel size is discover then this will be saved as the 'pixel_size'.
            * add an option to display_advancement or not.
            * split "zseries" according to z value
            * add NXdata information to display detector/data from root as image
        * move format version to 1.0

* app:
    * add patch-nx application to modify an existing `NXTomo`
        * add dark or flat series
        * modify frame type
    * tomoh52nx:
        * warning if we try to convert a file containing some `NXTomo` entry
        * create directories for output file if necessary
        * check write rights on output file
        * split "zseries" according to z value

* miscellaneous::
    * adopt 'black' coding style


0.3.4: 2020/10/05
-----------------

* converter: fix log

0.3.3: 2020/08/26
-----------------

* h5_to_nx:
    * add set-param option to let the user define some parameters values like energy if he knows it is missing (and avoid asking him n times).
* io: add management of hdf5 files from tomoscan.io.HDF5File

0.3.1: 2020/08/19
-----------------

* add field_of_view parameter

* add plugin management (allows user to define motor position value from a python script) - PR !19


0.3.0: 2020/03/20
-----------------

* app: add several option to define titles

* h5_to_nx:
    * add possibility to add plugins for defining new motor position
    * units: move distances to meter

* edf_to_nx:
    * units: move distances to meter


0.2.0: 2020/22/04
-----------------

* setup: add entry point on __main__

* converter
    * `h5_to_nx`: add a possible callback to give input

* doc
    * add API documentation
    * add tutorials for `tomoedf2nx` and `tomoh5tonx`


0.1.0: 2020/03/12
-----------------

* app
    * add application `tomoedf2nx`: convert acquisition using old bliss and EDF to .hdf5 file format, nexus (NXtomo) compliant format.
    * add application `tomoh5tonx`: convert acquisition using bliss/hdf5 to a nexus (NXtomo) compliant format.

* converter
    * add `h5_to_nx` function to convert from bliss .hdf5 to nexus (NXtomo) compliant format.
    * add `get_bliss_tomo_entries` function to return the bliss 'roor' entries (for now named 'tomo:basic', 'tomo:fullturn' ...)
